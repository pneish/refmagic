# Refmagic Reference Tracking Software

## Installation

This project uses the [Play! Framework](http://playframework.org). To install:

1. Download the Play! Framework. It has been tested on Play-1.2.4 which you can download as a [zip file](http://download.playframework.org/releases/play-1.2.4.zip) (58MB).
2. Extract the contents of the zip file somewhere on your computer.
3. Either clone this repository in the play-1.2.4 directory that you extracted in step 2, or download go to get source (above) and download as a zip file which you can extract inside the play-1.2.4 directory.
4. From the command line enter `play dependencies refmagic` which whill download and configure the required dependencies.
5. Copy the `conf/application.conf.sample` to `conf/application.conf` and make any changes you require. In particular, see the values for the admin user and password at the end of the file.
6. Start the refmagic application by running from the command line `play run refmagic`
7. You can now go the application at [http://localhost:9090/rm/](http://localhost:9090/rm/) where you can log in using the credentials in the `application.conf` file.

## Configuration

Most of the configuration options can be done throught the `application.conf` file. By default the application uses a H2 database that resides on a filesystem, but a mysql database can be configured in the configuration file. You might also want to change the port that the application runs on (defaults to 9090). For more information about configuring the options and how the framework works go to  the [Play! Framework site](http://playframework.org).



 
