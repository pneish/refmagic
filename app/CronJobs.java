import play.*;
import play.jobs.*;
 
import models.*;
import mylib.*;

 
@Every("1mn")
public class CronJobs extends Job {
 
	/**
	 * Loads clients from the LDAP directory into the client table.
	 * @See LdapImporter
	**/ 
    public void doJob() {
	
		if(Play.configuration.getProperty("ldap.sync_clients_from_ldap").equals("true")){
			
			System.out.println("Running scheduled update of client data!");
		
			LdapImporter li = new LdapImporter(Play.configuration.getProperty("ldap.provider_url"), 
											   Play.configuration.getProperty("ldap.user"),
											   Play.configuration.getProperty("ldap.password")); 

			li.updateClients("Member");
			li.updateClients("Electorate Officer");
		}	
    }
	
}