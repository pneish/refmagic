package controllers;
 
import play.*;
import play.mvc.*;

import models.Activity;
 
// have to use the annotation here because Play! only understands simple plurals 
@CRUD.For(Activity.class) 
@With(Secure.class)
public class Activities extends CRUD {    
}