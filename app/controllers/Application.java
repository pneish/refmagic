package controllers;

import play.*;
import play.mvc.*;
import play.data.validation.*;
import play.db.jpa.Blob;
import play.modules.paginate.ValuePaginator;
import play.libs.*;
import org.apache.commons.mail.*;


import java.util.*;
import java.io.File;
import java.io.FileNotFoundException;

import models.*;


@With(Secure.class) 
public class Application extends Controller {

    @Before
    static void addDefaults() {
        renderArgs.put("pageTitle", Play.configuration.getProperty("page.title"));
        renderArgs.put("pageBaseline", Play.configuration.getProperty("page.baseline"));
		renderArgs.put("appVersion", Play.configuration.getProperty("appVersion"));
    }
	
	@Before
    static void setConnectedUser() {
        if(Security.isConnected()) {
            StaffMember staff = StaffMember.find("byUsername", Security.connected()).first();
            if(staff != null){
				renderArgs.put("staff", staff.fullname);
				renderArgs.put("firstname", staff.fullname.split(" ")[0]);
				renderArgs.put("staffId", staff.id);
			}
			else{
				try{
					System.out.println("Something went wrong with login - best to get user to log in again");
					Secure.logout();
				}
				catch(Throwable e){
					System.out.println(e.toString());
				}	
			}	
        }
    }



	/**
	 * index displays the front page of the refmagic application. It loads a set of queries
	 * based on the filter provided.
	 *
	 * @param filter	which queries to display: use these filters to send a
	 *					canned search my - shows the logged in user, all - shows
	 *					everyone's queries, archive - shows all archived queries. Any other
	 *					term will be used as a general search term.
	 *
	 **/
    public static void index(String filter) {
	
		List<Query> queries = new ArrayList<Query>();
		
		String current = "";
		String listTitle = "";
		
		if( filter == null || filter.equalsIgnoreCase("my")){
		
			current = "my";
			listTitle = "My Queries";
		
			queries = Query.find(
				"staffMember.username = ? AND isArchived = false ORDER BY isCompleted, created desc", Security.connected()
				).from(0).fetch(Integer.parseInt(Play.configuration.getProperty("maxQueriesFrontPage")));
        }
		else if(filter.equalsIgnoreCase("all")){
		
			listTitle = "Everyone's Queries";
		
			current = "all";
		
			queries = Query.find("isArchived = false ORDER BY created desc")
			.from(0).fetch(Integer.parseInt(Play.configuration.getProperty("maxQueriesFrontPage")));
		}
		else if(filter.equalsIgnoreCase("archive")){
		
			listTitle = "Archived Queries";
		
			current = "all";
		
			queries = Query.find("isArchived = true ORDER BY created desc")
			.from(0).fetch(Integer.parseInt(Play.configuration.getProperty("maxQueriesFrontPage")));
		}
		else{
		
			current = "search";
			listTitle = "Search results for '";
			if(filter.length() > 50){
				listTitle += filter.substring(0,50) + "...";
			}
			else listTitle += filter;
			
			listTitle += "'";
			
			filter = filter.toLowerCase();
			filter = "%"+filter+"%";
			queries = Query.find(
				"lower(clientName) like ? OR lower(question) like ? OR lower(staffMember.fullname) like ?", filter, filter, filter)
				.from(0).fetch(Integer.parseInt(Play.configuration.getProperty("maxQueriesFrontPage")));
		}
		
		
		renderArgs.put("listTitle", listTitle);
		renderArgs.put("current", current);
        renderArgs.put("queries", queries);
		//renderArgs.put("myQueries", myQueries);
		
		
		// get the latest activity
		List<Activity> activities = Activity.find("Order By date desc").fetch(5);
		renderArgs.put("activities", activities);
		
		// and look up the categories
		List<Category> categories = Category.find("Order By category").fetch();
		renderArgs.put("categories", categories);
		
		ValuePaginator paginator = new ValuePaginator(queries);
		paginator.setPageSize(Integer.parseInt(Play.configuration.getProperty("numQueriesFrontPage")));
		renderArgs.put("paginator", paginator);
	
        render();
    }
	
	public static void form(Long id) {
		List<Category> categories = Category.find("Order By category").fetch();
		renderArgs.put("categories", categories);
		
		List<StaffMember> staffMembers = StaffMember.find("Order by fullname").fetch();
		renderArgs.put("staffMembers", staffMembers);
		
		if(id != null){
			
			Query q = Query.findById(id);
			
			render(q);
		}
		else {
				render();
		}	
	}
	
	/* renders any extra information (useful resources etc) to users */
	public static void extra(){
		render();
	}
    
    public static void save(Long id, 
							Long clientId, 
							@Required(message="Name is required") String clientName, 
							String clientEmail, 
							String clientPhone, 
							String clientCategory, 
							String clientDescription, 
							String question, 
							String answer, 
							String comment, 
							String due,
							Long staffId,
							Boolean isCompleted) {
        //Get the client
		Client c;
		
		if(clientId != null){
			c = Client.findById(clientId);
		}
		else{	
			c = new Client(clientEmail, clientName, clientCategory, clientPhone, clientDescription, Security.connected());
		}
		
		StaffMember s;
		if(staffId != null){
			s = StaffMember.findById(staffId);
		}
		else {
			s = StaffMember.find("byUsername", Security.connected()).first();
		}	
		
		System.out.println("found staff " + s);
		
		Query query;
		if(id != null){
			query = Query.findById(id);
			query.client = c;
			query.clientName = clientName;
			query.clientEmail = clientEmail;
			query.clientPhone = clientPhone;
			query.clientCategory = clientCategory;
			query.clientDescription = clientDescription;
			query.question = question;
			query.answer = answer;
			query.comment = comment;
			if (due.length() > 0) { query.due = new Date(due);}
			query.staffMember = s;
			if(isCompleted !=null) {query.isCompleted = isCompleted; }
			else { query.isCompleted = false; }
		}
		else {
			query = new Query(c, clientName, clientEmail, clientPhone, clientCategory, clientDescription, s, question ,answer, comment, due);
			
        }

        // Validate
        validation.valid(query);
		if(validation.hasErrors()) {
			 //index(null);
		}
        // Save
		c.save();
        query.save();

		
		// and add an activity to the logs	
		String activity = "";
		if(id!=null){
			activity = "Item edited";
		}
		else { activity = "Item created"; }
		Activity a = new Activity(s, query, activity);
		a.save();
		
        flash.success("Item saved!");
        index(null);
    }
	
	public static void ckUpload(String CKEditorFuncNum, File[] upload){
		String callback = CKEditorFuncNum;
		String msg = "";
		Resource r = null;
		
		// Store the attachment
			try{
				r = new Resource(upload[0]);
				r.save();
			}
			catch(FileNotFoundException e){
				System.out.println("No file: " + e);
				msg = "Error with file: " + e;
			}
			
		renderArgs.put("resource", r);	
		renderArgs.put("message", msg);
		renderArgs.put("callback", callback);

		
		render();
		//$output = '<html><body><script type="text/javascript">window.parent.CKEDITOR.tools.callFunction('.$callback.', "'.$url.'","'.$msg.'");</script></body></html>';
	}
	
	public static void batchDelete(Long[] ids){
		for (Long i : ids){
			deleteQuery(i);
		}
		if(ids.length == 1){ 
			flash.success("1 query deleted");
		}
		else {
			flash.success(ids.length + " queries deleted");
		}	
		index(null);
	}
	
	
	private static void deleteQuery(Long id){
		
		Query query = Query.findById(id);
		if(query != null){
			query.delete();
		}
		
	}
	
	public static void batchComplete(Long[] ids){
		//hmmm
		for (Long i : ids){
			markComplete(i);
		}
		if(ids.length == 1){ 
			flash.success("1 query marked complete");
		}
		else {
			flash.success(ids.length + " queries marked complete");
		}	
		index(null);
	}
	
	private static void markComplete(Long id){
	
		System.out.println("completing id: " + id);
		Query query = Query.findById(id);
		if(query != null){
			query.isCompleted = true;
			query.save();
		}
	}
	
	public static void batchArchive(Long[] ids){
		for (Long i : ids){
			Query q = Query.findById(i);
			if(q != null){
				q.isArchived = true;
				q.save();
			}
		}
		if(ids.length == 1){ 
			flash.success("1 query archived");
		}
		else {
			flash.success(ids.length + " queries archived");
		}	
		index(null);
	}
	
	public static void getResource(String uuid) {
		Resource r = Resource.find("uuid", uuid).first();
	    response.setContentTypeIfNotSet(r.blob.type());
	    renderBinary(r.blob.get(), r.name);
	}
	
	public static void deleteResource(Long id){
		Resource r = Resource.findById(id);
		//Query q = r.query;
		r.delete();
		index(null);
		//redirect("query/"+q.id);
	}
	
	 public static void queryList(String type, Long id, String filter){
	
	 	List<Query> queries = null;
		String qstring = "";
		
		Long q_user;
		// first the special cases
		if(type.equalsIgnoreCase("staff") && id > 0){
			qstring = "staffMember.id = " + id;
		}
		
		if(filter !=null){
			if(filter.equalsIgnoreCase("complete") || filter.equalsIgnoreCase("incomplete")){
				qstring += " AND completed = " + filter;
			}	
		}
		
		qstring += " ORDER BY created desc ";
	
		queries = Query.find(qstring).fetch();
		
		ValuePaginator paginator = new ValuePaginator(queries);
		render(paginator);
	}	
	
	public static void preview(Long id){
	
		Query q = Query.findById(id);
		
		render(q);
	
	}
	
	public static void emailForm(Long id){
		Query q = Query.findById(id);
		render(q);
	}
	
	public static void emailQuery(Long id, String subject, String body, List<String> sendTo ){
		// emails a query to a client or to the staff member or both
		// the sendTo list contains one or more of: self, client, reference
		
		Query q = Query.findById(id);
		
		try{
			HtmlEmail email = new HtmlEmail();
			// TODO make the To go to the client as well as the staff member
			if(sendTo.contains("self")){
				email.addTo(q.staffMember.email);
			}
			if(sendTo.contains("client")){
				email.addTo("client@example.com");
			}
			if(sendTo.contains("reference")){
				email.addTo(Play.configuration.getProperty("reference_email"));
			}
			
			email.setFrom(q.staffMember.email, q.staffMember.fullname);
			email.setSubject(subject);

			// set the html message
			email.setHtmlMsg("<html>" + "<div style='font-family: Helvetica, Arial, Sans Serif;' >" + body + "</div></html>");
			// set the alternative message
			email.setTextMsg("Your email client does not support HTML.");
			
			Mail.send(email); 
			flash.success("Email sent");
		}
		catch (EmailException e) {
			e.printStackTrace();
			flash.error("Problem sending email");
		}
		
		index(null);
	
	}
	
	
	

    

}