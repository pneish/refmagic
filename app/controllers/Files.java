package controllers;

import play.*;
import play.mvc.*;
import play.data.validation.*;
import play.db.jpa.Blob;

import java.util.*;
import java.io.File;
import java.io.FileNotFoundException;


import models.*;


public class Files extends Controller {
	
	public static void getResource(String uuid) {
	
		String newuuid = uuid.replaceFirst("\\[.*\\]", "");
		
		Resource r = Resource.find("uuid", newuuid).first();
	    response.setContentTypeIfNotSet(r.blob.type());
		response.cacheFor("5mn");
	    renderBinary(r.blob.get());

	}
  

}