package controllers;

import play.*;
import play.mvc.*;

import java.util.*;
import java.lang.annotation.*;

import models.*;
import models.StaffMember;

public class Ldap extends Controller {

    @Before
    static void checkAuthenticated() {
        if(session.contains("user")) {
            // The user is authenticated,
            // add User object to the renderArgs scope
            StaffMember authenticated = StaffMember.find("byUsername", session.get("user")).first();
            renderArgs.put("user", authenticated);
        } else {
            // The user is not authenticated,
            // redirect to the login form
            //Authentication.login();
			try{
					Secure.logout();
				}
				catch(Throwable e){
					System.out.println(e.toString());
				}	
        }
    }
    
    @Before
    static void checkAuthorization() {
        Admin adminAnnotation = getActionAnnotation(Admin.class);
        if(adminAnnotation != null) {
            // The action method is annotated with @Admin,
            // check the permission
            if(!renderArgs.get("user", StaffMember.class).isAdmin()) {
                // The connected user is not admin;
                forbidden("You must be admin to see this page");
            }
        }
    }
    
    @Target({ElementType.METHOD, ElementType.TYPE})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Admin {}
    
}