package controllers;

import play.*;
import play.mvc.*;
import play.data.validation.*;

import java.util.*;

import models.*;


public class Lookup extends Controller {

    
    public static void client(String term) {
        
		List <Client> clients = new ArrayList<Client>();
		// TODO - set the autolookup by config or by a passed parameter, and the list length below
		if(term.length() > 1){
			clients = Client.find("LOWER(fullname) like LOWER(?)", "%"+term+"%").fetch(10);
       
			renderJSON(clients);
		}
    }
    

    

}