package controllers;
 
import play.*;
import play.mvc.*;

import models.Query;
 
// have to use the annotation here because Play! only understands simple plurals 
@CRUD.For(Query.class) 
@With(Secure.class)
public class Queries extends CRUD {    
}