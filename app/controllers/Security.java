package controllers;
 
import javax.naming.*;
import javax.naming.directory.*;
import java.util.Hashtable;
import models.*;
import play.*;
 
public class Security extends Secure.Security
{
 
    static boolean authentify(String username, String password)
    {
		// create an admin if there are no staffMembers (should move this to a bootstrap job)
		if(StaffMember.count() == 0){
			
			StaffMember s = new StaffMember(Play.configuration.getProperty("admin_user"),
											Play.configuration.getProperty("admin_email"),
											Play.configuration.getProperty("admin_fullname"),
											Play.configuration.getProperty("admin_password"));
			s.save();								
		
		}
		
	    if(Play.configuration.getProperty("authenticationMethod").equals("ldap")){
			return  ldapAuthentify(username, password);
		}
		else if(Play.configuration.getProperty("authenticationMethod").equals("db")){
			 return  dbAuthentify(username, password);
		}
		else {
			return false;
		}	 
    }
	
	static boolean ldapAuthentify(String username, String password){
	
		boolean ret;
		ret = false;
	
		// first we need a username and password otherwise, I mean, what is the point?
		if (password.length() == 0){
			ret = false;
		}
		else{
		
			System.out.println("WE have credentials - lets test them.");
			
			Hashtable env = new Hashtable();
			env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
			env.put(Context.PROVIDER_URL, Play.configuration.getProperty("ldap.provider_url") );
			env.put(Context.SECURITY_AUTHENTICATION,"simple");
			env.put(Context.SECURITY_PRINCIPAL,"CN="+username+"," + Play.configuration.getProperty("ldap.security_principal")); // specify the username
			env.put(Context.SECURITY_CREDENTIALS, password);           // specify the password
			
			try {
				DirContext authContext = new InitialDirContext(env);
				System.out.println("Authentication Success! (for " + username + ")");

				
				// now get some attributes out of the directory
				Attributes attrs = authContext.getAttributes("CN=" + username +"," + Play.configuration.getProperty("ldap.admin_filter"));

				String mail = "";
				
				if(attrs.get("mail") == null){
					mail = attrs.get("givenName").get().toString() +"."+attrs.get("sn").get().toString()+"@p" + Play.configuration.getProperty("ldap.email_domain");
				}
				else{
					mail = attrs.get("mail").get().toString();
				}
				
				String fullname = attrs.get("description").get().toString();
			
				//create the StaffMember if they don't exist already				
				if(StaffMember.count("username = ?", username) < 1){
					new StaffMember(username, mail, fullname).save();	
				}	
				
				ret = true;
				session.put("user", username);
				
				
			} catch (AuthenticationException authEx) {
				System.out.println("Authentication failed!: "+authEx.toString());
				ret = false;
	 
			} catch (NamingException namEx) {
				System.out.println("Something went wrong!");
				namEx.printStackTrace();
				ret = false;
			}
		}
		
		return ret;
	}
	
	static boolean dbAuthentify(String username, String password){
		System.out.println("DB authentication FTW!");
		return true;
	}

	static void onAuthenticated() { 
    		String url = play.mvc.Router.reverse( "Application.index").toString(); 
    		flash.put( "url", url); 
	} 

}







