package controllers;
 
import play.*;
import play.mvc.*;
import models.*;
import java.util.*;
 
public class Stats extends CRUD {    
	public static void addStat(){
		StaffMember staff = StaffMember.find("byUsername", Security.connected()).first();
		Stat s = new Stat(staff);
		s.save();
		renderJSON(s);
	}
	
	public static void undoStat(Long id){
		Stat s = Stat.findById(id);
		s.delete(); 		
	}
	
	public static void statCount(){
		Date n = new Date();
		Calendar c = Calendar.getInstance();
		
		// today
		c.set(Calendar.HOUR, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 1);
		c.set(Calendar.AM_PM, 0);

		long today = Stat.count("date >= ?", c.getTime());
		
	
		
		// this month
		c.set(Calendar.DAY_OF_MONTH, 1);
		long thismonth = Stat.count("date >= ?", c.getTime());
		
		// this year
		c.set(Calendar.MONTH, 0);
		long thisyear = Stat.count("date >= ?", c.getTime());		
		long all = Stat.count();
		
		Map<String, Long> map = new HashMap<String, Long>();
		
		map.put("today", today);
		map.put("month", thismonth);
		map.put("year", thisyear);
		
		renderJSON(map);
	}
}