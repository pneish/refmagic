package models;
 
import java.util.*;
import javax.persistence.*;
 
import play.db.jpa.*;
 
/* tracks activity in the system - for simple logging purposes */
 
@Entity
public class Activity extends Model {
 
    public Date date;
    
    @ManyToOne
    public StaffMember staffMember;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = Query.class)
	public Query query;
	
	public String action;
		
    
    public Activity(StaffMember staffMember, Query query, String action) {
        this.staffMember = staffMember;
        this.date = new Date();
		this.query = query;
		this.action = action;
    }
	
	public String toString(){
		return this.date.toString() + " : " + this.action;
	}	
    
 
}