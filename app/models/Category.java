package models;
 
import java.util.*;
import javax.persistence.*;
 
import play.db.jpa.*;
 
/* stores extremely simple stats for reference requests */
 
@Entity
public class Category extends Model {
 
    public Date date;
	public String category;

    
    public void Category(String cat) {
        this.category = cat;
        this.date = new Date();
    }
    
	public String toString(){
		return this.category;
	}	
 
}