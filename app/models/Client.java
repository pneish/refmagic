package models;
 
import java.util.*;
import javax.persistence.*;
 
import play.db.jpa.*;
 
@Entity
public class Client extends Model {
 
    public String email;
    public String fullname;
    public String phone;
    public String type;
    public String description;
	public String notes;
	public String source; 	// from LDAP or entered manually
	public Date updated = new Date();
    
    public Client(String email, String fullname, String type, String phone, String description, String source) {
        this.email = email;
        this.fullname = fullname;
        this.type =  type;
        this.phone = phone;
		this.description = description;
		this.source = source;
    }
    
    public String toString() {
        return this.fullname;
    }
    
 
}