package models;
 
import java.util.*;
import javax.persistence.*;
import java.text.ParseException;
import java.text.SimpleDateFormat; 

import java.io.File;
import java.io.FileNotFoundException;
 
import play.db.jpa.*;
import play.libs.MimeTypes;
import play.db.jpa.Blob;
import play.data.validation.*;
 
@Entity
public class Query extends Model {
 
	// Notes these are properties of the query, not the client
	// we do this so that changes in the clients persist over time and are
	// tied to the particular query

	public String clientName;
	public String clientEmail;
	public String clientPhone;
	public String clientCategory;
	public String clientDescription;
	
	// but we also link to the client so that we can track 
	@ManyToOne
	public Client client;
    
    public Date created;
    public Date updated;
	
    public Date due;
    
	public Date completed;
    
    @Lob
    @Required
    @MaxSize(10000)
    public String question;
    
    @Lob
    @MaxSize(10000)
    public String answer;
    
    @MaxSize(10000)
    public String comment;
    
    @ManyToOne
    public StaffMember staffMember;
    
    //@OneToMany(mappedBy="query", cascade=CascadeType.ALL)
    //public List<Resource> resources;
    
    public boolean isArchived;
    public boolean isCompleted;
	public boolean isSignificant; //i.e. equivalent to RKB
	
	@OneToMany(cascade = CascadeType.ALL, targetEntity = Activity.class, mappedBy = "query")
	public List<Activity> activities;
	

    
    public Query(Client client, String clientName, String clientEmail, String clientPhone, String clientCategory, String clientDescription, StaffMember staffMember, String question, String answer, String comment, String due) {
		this.client = client;
        //this.resources = new ArrayList<Resource>();
        this.clientName = clientName;
		this.clientEmail = clientEmail;
		this.clientPhone = clientPhone;
		this.clientCategory = clientCategory;
		this.clientDescription = clientDescription;
        this.staffMember = staffMember;
        this.question = question;
        this.answer = answer;
		this.comment = comment;
        this.created = new Date();
        this.updated = new Date();
        this.isArchived = false;
        this.isCompleted = false;
		
		// and try and add the due date
		if(due != null){
			SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
			try{
				this.due = formatter.parse(due);
			}
			catch(ParseException e){
				e.printStackTrace();
			}
		}
		
		// and add a stat while we are here
		new Stat(staffMember).save();
		
    }
 
 /*
    public Query addResource(File attachment) throws FileNotFoundException {
		
        Resource newResource = new Resource(this, attachment);
		newResource.save();
        this.resources.add(newResource);
        this.updated = new Date();
        this.save();
        return this;
    }
	
	public Query deleteResource(Long id){
		Resource r = Resource.findById(id);
		r.delete();
		return this;
	}
 */   
    public Query archive(){
        this.isArchived = true;
        this.save();
        return this;
    }
    
    public Query createQuery(){
        return this;
        // TODO - Stub
    }    
	
	
   
    @PreUpdate
    public void update(){
        this.updated = new Date();
        System.out.println("updated "+ this.updated.toString());
    }
    
    public String toString() {
        return this.question;
    }
    
 
}