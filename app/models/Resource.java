package models;
 
import java.util.*;
import javax.persistence.*;
import java.net.*;
 
import play.db.jpa.*;
import play.db.jpa.Blob;
import play.libs.MimeTypes;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;


@Entity
public class Resource extends Model {
 
    public String name;
    public String mimeType;
	public String uuid;
	public Blob blob;
    
    //@ManyToOne
    //public Query query;
    
    public Resource(File attachment) throws FileNotFoundException {
        //this.query = query;
		this.name = attachment.getName();
		this.blob = new Blob();
		this.blob.set(new FileInputStream(attachment), MimeTypes.getContentType(attachment.getName()));
		this.mimeType = MimeTypes.getContentType(attachment.getName());
		this.uuid = blob.getFile().getName();
    }
	
	public Resource(){
	}
	


	
	@PreRemove
	public void deleteBlob() {
	   blob.getFile().delete();
	}
	
}