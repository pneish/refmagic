package models;
 
import java.util.*;
import javax.persistence.*;
 
import play.db.jpa.*;
import play.data.validation.*;

import play.libs.Crypto;
import play.libs.Codec;

 
@Entity
public class StaffMember extends Model {
 
    @Email
    @Required
    public String email;
    
    @Required
    public String fullname;
	
	@Required
	public String username;
	
	public Boolean admin;
	
	// if we are authenticating against our own database
	public String password_hash;
	public String salt;
	
    
    public StaffMember(String username, String email, String fullname) {
		this.username = username;
        this.email = email;
        this.fullname = fullname;
    }
	
	public StaffMember(String username, String email, String fullname, String password) {
		this.username = username;
        this.email = email;
        this.fullname = fullname;
		this.salt = Codec.UUID();
		this.password_hash = Crypto.passwordHash(password + this.salt);
	}
	
    public String toString() {
        return this.fullname + " (" + this.email + ")";
    }
	
	public Boolean checkPassword(String password){
		if(Crypto.passwordHash(password + this.salt) == this.password_hash){
			return true;
		}
		else{
			return false;
		}	
	}
	
	public Boolean isAdmin(){
		// everyone is an admin for now
		return true;
	}
    
 
}