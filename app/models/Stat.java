package models;
 
import java.util.*;
import javax.persistence.*;
 
import play.db.jpa.*;
 
/* stores extremely simple stats for reference requests */
 
@Entity
public class Stat extends Model {
 
    public Date date;

    
    @ManyToOne
    public StaffMember staffMember;
    
    public Stat(StaffMember staffMember) {
        this.staffMember = staffMember;
        this.date = new Date();
    }
	
	public String toString(){
		return this.date.toString();
	}	
    
 
}