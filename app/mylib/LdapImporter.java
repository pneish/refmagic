package mylib;

import java.util.*; 
import javax.naming.*;
import javax.naming.directory.*;
import java.util.Hashtable;
import models.*;
import play.*;
 
// retrieves client entries from an LDAP directory 
public class LdapImporter
{
	String username;
	String password;
	String start_url;
	
	public LdapImporter(String start_url, String user, String password)
	{
			this.start_url = start_url;
			this.username = user;
			this.password = password;			
	}		
 
    public List <Client> getClients(String searchfor)
    {
		List <Client> clientlist = new ArrayList<Client>();
		boolean ret = false;
		
     	Hashtable env = new Hashtable();
		env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, this.start_url);
		env.put(Context.SECURITY_AUTHENTICATION,"simple");
		env.put(Context.SECURITY_PRINCIPAL,"CN="+this.username+"," + Play.configuration.getProperty("ldap.allusers"));
		env.put(Context.SECURITY_CREDENTIALS, this.password);           // specify the password
		
		System.out.println(Play.configuration.getProperty("ldap.allusers"));
	
		try {
			DirContext authContext = new InitialDirContext(env);
			System.out.println("Authentication Success!");
			
			// now get some attributes out of the directory
			NamingEnumeration answer = authContext.search(Play.configuration.getProperty("ldap.provider_url"), "(title="+searchfor+")", getSimpleSearchControls());
			
			 while ( answer.hasMoreElements() ) {
				SearchResult result = (SearchResult) answer.next();    
				Attributes attrs = result.getAttributes();
				//System.out.println(attrs.get("telephoneNumber"));
				//System.out.println(attrs.get("mail"));
				//System.out.println(attrs.get("description"));
				//System.out.println(attrs.get("department"));
				//System.out.println(attrs.get("physicalDeliveryOfficeName"));


				// now update or add clients to database
				// we'll do this using the email address as the Primary Key
				
				String email = (attrs.get("mail") == null) ?  "" : attrs.get("mail").get().toString(); 
				String fullname = (attrs.get("description") == null) ?  "" : attrs.get("description").get().toString(); 
				String phone = (attrs.get("telephoneNumber") == null) ?  "" : attrs.get("telephoneNumber").get().toString(); 
				String description = (attrs.get("physicalDeliveryOfficeName") == null) ?  "" : attrs.get("physicalDeliveryOfficeName").get().toString(); 
				description += (attrs.get("department") == null) ?  "" : " - " + attrs.get("department").get().toString(); 
				String source = "LDAP";
				String type = (attrs.get("title") == null) ?  "" : attrs.get("title").get().toString(); ;
				
				if(email.length() > 0){			
					clientlist.add(new Client(email, fullname, type, phone, description, source));
					//System.out.println("Found Client: " + email);
				}
				
				
			} 
			answer.close();
			
			
		
						
		} catch (AuthenticationException authEx) {
			System.out.println("Authentication failed!: "+authEx.toString());
			ret = false;
 
		} catch (NamingException namEx) {
			System.out.println("Something went wrong!");
			namEx.printStackTrace();
			ret = false;
		}

		
		return clientlist;
    }
	
	public void updateClients(String type){
		// updates the database with the current client information
				
		List<Client> clients = this.getClients(type);
		
		Iterator i = clients.iterator();
		while(i.hasNext()){
			Client ldap_client = (Client) i.next();
			
			if(Client.count("email = ?", ldap_client.email) > 0){
				Client current_client = Client.find("byEmail", ldap_client.email).first();
				current_client.fullname = ldap_client.fullname;
				current_client.type = ldap_client.type;
				current_client.phone = ldap_client.phone;
				current_client.description = ldap_client.description;
				current_client.save();
				//System.out.println("Existing client: " + ldap_client.email);
			}
			else{
				// new client
				ldap_client.save();
				System.out.println("new client: " + ldap_client.email);
			}			
		}
	}
	
	private SearchControls getSimpleSearchControls() {
		SearchControls searchControls = new SearchControls();
		searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		searchControls.setTimeLimit(30000);
		//String[] attrIDs = {"objectGUID"};
		//searchControls.setReturningAttributes(attrIDs);
		return searchControls;
	}
}
